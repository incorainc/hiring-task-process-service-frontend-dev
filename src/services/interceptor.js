import axios from 'axios';

export const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_URL,
});

const handleRequest = (request) => {
    request.headers['Authorization'] = `Bearer testtoken`;
    return request;
};
axiosInstance.interceptors.request.use(req => handleRequest(req));
axiosInstance.interceptors.response.use(
    res => res,
    error => {
        console.log(error)
        return Promise.reject(error.response.data);
    },
);
