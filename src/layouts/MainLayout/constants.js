export const navigationList = [
    {
        description: "Data Catalog",
        icon: "data-catalog.svg"
    },
    {
        description: "Processes",
        icon: "processes.svg"
    },
    {
        description: "Incidents",
        icon: "incidents.svg"
    },
]