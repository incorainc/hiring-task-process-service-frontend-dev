import styles from './MainLayout.module.scss';
import {useState, useMemo} from 'react';
import { navigationList } from './constants';
import { DataCatalog } from '../../components/DataCatalog';
import { Processes } from '../../components/Processes';
import { Incidents } from '../../components/Incidents';
import { NewProcessForm } from '../../components/NewProcessForm';
export const MainLayout = () => {
    const [tabIndex, setTabIndex] = useState(0);
    const [isFormOpen, setFormOpen] = useState(false)
    const [processDetailId, setProcessDetailId] = useState(null);
    const currentTab = useMemo(() => {
        switch (tabIndex) {
            case 0:
                return <DataCatalog/>
            case 1:
                return <Processes 
                    isFormOpen={isFormOpen} 
                    openForm={() => setFormOpen(true)}
                    setProcessDetailId={setProcessDetailId}
                />
            case 2:
                return <Incidents/>
            default:
                <></>;
        }
    }, [tabIndex, isFormOpen])
    return <div className={`${styles.mainLayoutWrapper}`}>
        <aside className={`${styles.mainLayoutNavigation}`}>
            <div className={`${styles.title}`}>RecoLabs</div>
            {navigationList.map((tab, index) => (
                <button 
                    key={index}
                    onClick={(e)=>setTabIndex(index)}
                    className={`${styles.navigationTab} ${tabIndex === index && styles.active}`}
                >
                    <img src={`./icons/${tab.icon}`} alt={tab.description}/>
                    {tab.description}
                </button>
            ))}
        </aside>
        <div className={`${styles.mainLayoutContent}`}>{currentTab}</div>
        <NewProcessForm 
            processId={processDetailId} 
            closeForm={() => setFormOpen(false)}  
            isOpen={isFormOpen}
        />
    </div>
}