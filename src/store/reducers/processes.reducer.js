import { processesTypes } from "../actions/processes.action"

const initialState = {
    loading: false,
    myProcesses: [],
    recommendedProcesses: [],
    cardLoading: false,
    cardName: ''
}

export const ProcessesReducer = (state=initialState, action) => {
    switch (action.type){
        case processesTypes.EDIT_PROCESS_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case processesTypes.EDIT_PROCESS_SUCCESS:
            return {
                ...state,
                loading: false,
                recommendedProcesses: state.recommendedProcesses.map((pr) => pr.id === action.payload.id ? action.payload : pr)
            };
        case processesTypes.EDIT_PROCESS_FAILURE:
            return {
                ...state,
                loading: false,
            };
        case processesTypes.POST_NEW_PROCESS_REQUEST:
            return {
                ...state,
                cardName: action.payload.name,
                cardLoading: true
            }
            case processesTypes.POST_NEW_PROCESS_SUCCESS: 
            return {
                ...state,
                myProcesses: [...state.myProcesses, action.payload],
                cardName: '',
                cardLoading: false
            }
        case processesTypes.POST_NEW_PROCESS_FAILURE:
            return {
                ...state,
                cardName: '',
                cardLoading: false
            }
        case processesTypes.FETCH_ALL_PROCESSES_FAILURE:
            return {
                ...state,
                loading: false
            }
        case processesTypes.FETCH_ALL_PROCESSES_REQUEST: 
            return {
                ...state,
                loading: true
            }
        case processesTypes.FETCH_ALL_PROCESSES_SUCCESS:
            return {
                ...state,
                myProcesses: action.payload.myProcesses,
                recommendedProcesses: action.payload.recommendedProcesses,
                loading: false
            }
        default:
            return state
    }
}