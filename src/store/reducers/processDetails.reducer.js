import { processDetailsTypes } from "../actions/processDetails.action";

const initialState = {
    loading: false,
    processDetails: null
}

export const ProcessDetailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case processDetailsTypes.GET_PROCESS_DETAILS_REQUEST:
            return {
                ...state,
                loading: true,
            };
        case processDetailsTypes.GET_PROCESS_DETAILS_SUCCESS:
            return {
                ...state,
                loading: false,
                processDetails: action.payload,
            };
        case processDetailsTypes.GET_PROCESS_DETAISL_FAILURE:
            return {
                ...state,
                loading: false,
            };
        default:
            return state;
    }
}