import { call, all, takeLatest, put } from 'redux-saga/effects';
import { axiosInstance } from '../../services/interceptor';
import { 
    processesTypes ,
    getAllProcessesMetadataFailureAction,
    getAllProcessesMetadataSuccessAction,
    postNewProcessSuccessAction,
    postNewProcessFailureAction,
    editProcessSuccessAction,
    editProcessFailureAction
} from '../actions/processes.action';


export function* postNewProcess(action) {
    try {
        yield call(axiosInstance.post, '/process-metadata', {md:action.payload});
        yield put(postNewProcessSuccessAction(action.payload));
    } catch (error) {
        yield put(postNewProcessFailureAction(error.message));
    }
}

export function* editProcess(action) {
    try {
        yield call(axiosInstance.put, `/process-metadata`, {md:action.payload});
        yield put(editProcessSuccessAction(action.payload));
    } catch (error) {
        yield put(editProcessFailureAction(error.message ));
    }
}

function* getAllProcessesMetadata() {
    try {
        const {data} = yield call(axiosInstance.get, '/process-metadata');
        const reports = data.reportStatus
            .filter(({status}) => status === 'REPORT_GENERATION_STATUS_UNSPECIFIED')
            .map((elem) => elem.processId);
        
        const allProcesses = {
            myProcesses: data.mds.filter((proc) => !reports.includes(proc.id)),
            recommendedProcesses: data.mds.filter((proc) => reports.includes(proc.id))
        }
        yield put(getAllProcessesMetadataSuccessAction(allProcesses));
    } catch (error) {
        yield put(getAllProcessesMetadataFailureAction(error.message));
    }
}

export function* processesSaga(){
    yield all([
        takeLatest(processesTypes.POST_NEW_PROCESS_REQUEST, postNewProcess),
        takeLatest(processesTypes.FETCH_ALL_PROCESSES_REQUEST, getAllProcessesMetadata),
        takeLatest(processesTypes.EDIT_PROCESS_REQUEST, editProcess)
    ])
}