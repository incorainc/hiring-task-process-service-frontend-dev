import { processDetailsTypes } from "../actions/processDetails.action"
import { call, all, takeLatest, put } from 'redux-saga/effects';
import { axiosInstance } from '../../services/interceptor';
import { getProcessDetailsSuccessAction, getProcessDetailsFailureAction } from "../actions/processDetails.action";

export function* getProcessDetails(action){
    try {
        const {data} = yield call(axiosInstance.get, `/process-metadata/${action.payload}`);
        const {anchors, ...otherFields} = data.md;
        yield put(getProcessDetailsSuccessAction({ ...otherFields, ...anchors}));
    } catch (error) {
        yield put(getProcessDetailsFailureAction(error.message));
    }
}

export function* processDetailsSaga(){
    yield all([
        takeLatest(processDetailsTypes.GET_PROCESS_DETAILS_REQUEST, getProcessDetails)
    ])
}