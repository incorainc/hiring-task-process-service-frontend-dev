import { all } from 'redux-saga/effects'
import {processesSaga} from './processes.saga'
import { processDetailsSaga } from './processDetails.saga'

export default function* rootSaga(){
    yield all([
        processesSaga(),
        processDetailsSaga()
    ])
}