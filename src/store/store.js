import { applyMiddleware,  createStore, combineReducers, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import {ProcessesReducer} from './reducers/processes.reducer';
import { ProcessDetailsReducer } from './reducers/processDetails.reducer';
import rootSaga from './sagas';

const sagaMiddleware = createSagaMiddleware();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const rootReducer = combineReducers({
    processes: ProcessesReducer,
    processDetails: ProcessDetailsReducer,
});

export const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);

sagaMiddleware.run(rootSaga);
