export const processesTypes = {
    FETCH_ALL_PROCESSES_REQUEST: 'FETCH_ALL_PROCESSES_REQUEST',
    FETCH_ALL_PROCESSES_SUCCESS: 'FETCH_ALL_PROCESSES_SUCCESS',
    FETCH_ALL_PROCESSES_FAILURE: 'FETCH_ALL_PROCESSES_FAILURE',
    POST_NEW_PROCESS_REQUEST: 'POST_NEW_PROCESS_REQUEST',
    POST_NEW_PROCESS_SUCCESS: 'POST_NEW_PROCESS_SUCCESS',
    POST_NEW_PROCESS_FAILURE: 'POST_NEW_PROCESS_FAILURE',
    EDIT_PROCESS_REQUEST: 'EDIT_PROCESS_REQUEST',
    EDIT_PROCESS_SUCCESS: 'EDIT_PROCESS_SUCCESS',
    EDIT_PROCESS_FAILURE: 'EDIT_PROCESS_FAILURE',
}

export const editProcessSuccessAction = (process) => ({
    type: processesTypes.EDIT_PROCESS_SUCCESS,
    payload: process
})

export const editProcessFailureAction = (message) => ({
    type: processesTypes.EDIT_PROCESS_FAILURE,
    payload: message
})

export const editProcessRequestAction = (process) => ({
    type: processesTypes.EDIT_PROCESS_REQUEST, 
    payload: process
})
    
export const postNewProcessSuccessAction = (process) => ({
    type: processesTypes.POST_NEW_PROCESS_SUCCESS,
    payload: process
})

export const postNewProcessFailureAction = (message) => ({
    type: processesTypes.POST_NEW_PROCESS_FAILURE,
    payload: message
})

export const postNewProcessRequestAction = (process) => ({
    type: processesTypes.POST_NEW_PROCESS_REQUEST, 
    payload: process
})

export const getAllProcessesMetadataRequestAction = () => ({
    type: processesTypes.FETCH_ALL_PROCESSES_REQUEST
})

export const getAllProcessesMetadataFailureAction = (message) => ({
    type: processesTypes.FETCH_ALL_PROCESSES_FAILURE,
    payload: message
})

export const getAllProcessesMetadataSuccessAction = (processes) => ({
    type: processesTypes.FETCH_ALL_PROCESSES_SUCCESS, 
    payload: processes
})