export const processDetailsTypes = {
    GET_PROCESS_DETAILS_REQUEST: 'GET_PROCESS_DETAILS_REQUEST',
    GET_PROCESS_DETAILS_SUCCESS: 'GET_PROCESS_DETAILS_SUCCESS',
    GET_PROCESS_DETAILS_FAILURE: 'GET_PROCESS_DETAILS_FAILURE',
}

export const getProcessDetailsRequestAction = (id) => ({
    type: processDetailsTypes.GET_PROCESS_DETAILS_REQUEST,
    payload: id
})

export const getProcessDetailsFailureAction = (message) => ({
    type: processDetailsTypes.GET_PROCESS_DETAILS_FAILURE,
    payload: message
})

export const getProcessDetailsSuccessAction = (processDetails) => ({
    type: processDetailsTypes.GET_PROCESS_DETAILS_SUCCESS, 
    payload: processDetails
})