const Paths =  {
    Processes: '/processes',
    Incidents: '/incidents',
    DataCatalog: '/data-catalog'
}

export default Paths;
