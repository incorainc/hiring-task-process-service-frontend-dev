import { Field } from 'formik';
import styles from './ProcessAnchorField.module.scss';
import fieldStyle from '../NewProcessForm/NewProcessForm.module.scss';
import { useState } from 'react';
export const ProcessAnchorField = ({anchor}) => {
    const [isOpen, setOpen] = useState(false)  
    return <div className={`${styles.anchorField} ${isOpen && styles.isOpen}`}>
        <div className={`${styles.anchorFieldHead}`} onClick={()=>setOpen((prev => !prev))}>
            <img src='./icons/vector.svg' alt="arrow"/>
            {anchor.label}
        </div>
        <div className={`${styles.anchorFieldBody} ${fieldStyle.newProcessInputWrapper}`}>
            <Field
                name={anchor.name}
                as="textarea"
                placeholder={anchor.placeholder}
            />
        </div>
    </div>
}