import styles from './ProcessCard.module.scss';
import { Loader } from '../Loader';

export const ProcessCard = ({loading, description, name, iconUrl, recommended, editCard}) => {
    
    return <div className={`${styles.processCard} ${recommended && styles.recommended} ${loading && styles.loading}`}
        onClick={editCard ? editCard : null}
    >
        <div className={`${styles.processCardTitle}`}>{name}</div>
        <div className={`${styles.processCardDescription}`}>
            {loading ? 'Process creation in progress now' : description }
        </div>
        <div className={`${styles.processCardIcon}`}>
            {loading ? <Loader/> : <img src={iconUrl} alt='icon'/>}
        </div>
    </div>
}