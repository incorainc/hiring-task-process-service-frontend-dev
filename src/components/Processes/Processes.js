import styles from './Processes.module.scss';
import { useEffect } from 'react';
import { ProcessCard } from '../ProcessCard/ProcessCard';
import { useDispatch,  useSelector } from 'react-redux';
import { getAllProcessesMetadataRequestAction } from '../../store/actions/processes.action';

export const Processes = ({isFormOpen, openForm, setProcessDetailId}) => {
    const dispatch = useDispatch();
    const {recommendedProcesses, myProcesses, cardLoading, cardName} = useSelector(state => state.processes)

    useEffect(() => {
        dispatch(getAllProcessesMetadataRequestAction())
    }, []);

    return <div className={`${styles.processes}`}>
        <div className={`${styles.processesHeader}`}>
            <div className={`${styles.processesTitle}`}>
                <img src="./icons/process-logo.svg" alt='Logo'/>
                <div>Process Library</div>
            </div>
            <div className={`${styles.processesCalendar}`}>
                <img src='./icons/calendar.svg' alt='Calendar'/>
                <div>10/06/2021 - 17/06/2021</div>   
            </div>
        </div>
        <div className={`${styles.processesList}`}>
            <button disabled={isFormOpen} onClick={() => {
                openForm()
                setProcessDetailId(null)
            }}>Create new</button>  
            <div className={`${styles.myProcesses}`}>
                <span>My processes</span>
                <div>
                    {myProcesses.map(({description, name, iconUrl, id}, index) => (
                        <ProcessCard
                            description={description}
                            name={name}
                            iconUrl={iconUrl}
                            key={index}
                        /> 
                    ))}
                    {cardLoading && <ProcessCard name={cardName} loading={cardLoading} /> }
                    <ProcessCard name={'test card'} loading={true} />
                </div>   
            </div>  
            <div className={`${styles.recommendedProcesses}`}>
                <span>Recommended</span>
                <div>
                    {recommendedProcesses.map(({description, name, iconUrl, id}, index) => (
                        <ProcessCard
                            recommended
                            editCard={()=>{
                                setProcessDetailId(id)
                                openForm()
                            }}
                            description={description}
                            name={name}
                            iconUrl={iconUrl}
                            key={index}
                        /> 
                    ))}
                </div>  
            </div>  
        </div>
    </div>
}