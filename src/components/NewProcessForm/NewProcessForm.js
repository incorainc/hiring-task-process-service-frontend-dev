
import { Field, Form, Formik, ErrorMessage } from 'formik';
import styles from './NewProcessForm.module.scss';
import { useOnClickOutside } from '../../hooks/useClickOutside';
import {useCallback, useEffect, useMemo , useRef} from 'react';
import { processFormAnchors, initialProcessFormValues } from './constants';
import { ProcessAnchorField } from '../ProcessAnchorField';
import { v4 as uuidv4 } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { editProcessRequestAction, postNewProcessRequestAction } from '../../store/actions/processes.action';
import { getProcessDetailsRequestAction } from '../../store/actions/processDetails.action';
import { validationSchema } from './constants';


const FileField = ({...props}) => (
    <input 
        {...props} 
        value={undefined} 
        name="iconUrl" 
        id="file"
        onClick={e => (e.target.value = null)} 
        type="file" />
)

export const NewProcessForm = ({processId, isOpen, closeForm }) => {
    const ref = useRef();
    const dispatch = useDispatch();
    const {cardLoading} = useSelector(state => state.processes);
    const {processDetails} = useSelector(state => state.processDetails)

    const formikValues = useMemo(() => processId 
        ? {...initialProcessFormValues, ...processDetails } 
        : initialProcessFormValues,
    [processId])

    useEffect(()=>{
        if(processId) dispatch(getProcessDetailsRequestAction(processId))
    },[processId])

    const onSubmit = useCallback(({domains, relatedTerms, teams, users, ...formFields}, {resetForm}) => {
        const anchors = {domains, relatedTerms, teams, users}

        for (const key in anchors) {
            if(!anchors[key]) delete anchors[key]
            else anchors[key] = anchors[key].split(',')
        }

        const formData = {
            id: uuidv4(),  
            ...formFields,
            anchors,
        }
        processId ? dispatch(editProcessRequestAction(formData)) : dispatch(postNewProcessRequestAction(formData))
        resetForm()
        closeForm()
    }, [processId])
    
    useOnClickOutside(ref, closeForm);

    return <div ref={ref} className={`${styles.newProcess} ${isOpen && styles.showForm}`}>
        <div className={`${styles.newProcessBg}`}/>
        <Formik
            enableReinitialize
            initialValues={formikValues}
            validationSchema={processId ? null : validationSchema}
            onSubmit={onSubmit}>
            {({setFieldValue}) => {
                return <Form className={`${styles.newProcessForm}`}>
                <div className={`${styles.newProcessTitle}`}>{processId ? 'Edit' : 'Create New'} Process</div>
                <div className={`${styles.newProcessInputWrapper}`}>
                    <Field
                        id="name"
                        name="name"
                        placeholder="Process Name (mandatory)"
                    />
                    <ErrorMessage className={styles.error} name="name" component="div"/>
                </div>
                <div className={`${styles.newProcessAnchors}`}>
                    <p>Select one item from thie list below and provide us with the information we need in order to create your new process.</p>
                    {processFormAnchors.map((anchor, index) =>  <ProcessAnchorField key={index} anchor={anchor}/>)}
                </div>
                <div className={`${styles.newProcessInputWrapper}`}>
                    <Field
                        name="description"
                        as="textarea"
                        placeholder="Process Description (Optional)"
                    />
                    <ErrorMessage className={styles.error} name="description" component="div"/>
                </div>
                <label className={`${styles.newProcessFile}`} htmlFor="file">
                    <img src='./icons/plus.svg' alt='+'/>
                    <Field
                        // onChange={(event) => setFieldValue("iconUrl", event.target.files[0])}
                        onChange={() => setFieldValue("iconUrl", "https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/How_to_use_icon.svg/2214px-How_to_use_icon.svg.png")}
                        as={FileField}
                        name="iconUrl"
                    />
                </label>
                <ErrorMessage className="dsdasdsa" name="iconUrl" component="div"/>
                <button
                    disabled={cardLoading}
                    className={`${styles.newProcessSubmit}`}
                    type="submit">
                    {processId ? 'Edit' : 'Save'}
                </button>
            </Form>}}
        </Formik>
    </div>
}