import * as Yup from 'yup';

export const initialProcessFormValues = {
    name: '',
    description: '',
    iconUrl: '',
    teams: '',
    users: '',
    domains: '',
    relatedTerms: ''
};

export const validationSchema = Yup.object({
    name: Yup.string()
        .required('Required')
        .min(2, 'Minimum 2 characters.'),
    description: Yup.string()
        .required('Required')
        .min(2, 'Minimum 2 characters.'),
    iconUrl: Yup.string().required('Required')
})

export const processFormAnchors = [
    {
        name: 'domains',
        label: 'Relevant user groups',
        placeholder: 'Add at least 2 of the user groups associated with this process, separated by commas'
    },
    {
        name: 'relatedTerms',
        label: 'Relevant domains',
        placeholder: 'Add at least 2 outside Domains (3rd-Paries) associated with this process, separated by commas'
    },
    {
        name: 'teams',
        label: 'Relevant usernames',
        placeholder: 'Add at least one username associated with this process, separated by commas'
    },
    {
        name: 'users',
        label: 'Related terms',
        placeholder: 'Add at least 2 related terms associated with this process, separated by commas'
    }
]