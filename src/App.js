import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import { MainLayout } from "./layouts/MainLayout";

export const App = () =>  {
  return <BrowserRouter>
    <Routes>
      <Route path="/" element={<MainLayout/>}/>
    </Routes>
  </BrowserRouter>
}